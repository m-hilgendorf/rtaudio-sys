/*!
    # rtaudio-sys #

    Raw bindings to the RtAudio library, to interface with low-latency drivers on Windows, MacOS,
    and Linux/BSD.

    Currently supported Driver APIs are Direct Sound, WASAPI, CoreAudio, and ASIO.

    To support Linux drivers we need to reimplement the build steps on Linux, find the libraries
    for JACK, ALSA, OSS, and Pulse.
*/
#![allow(dead_code)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

use std::os::raw::{c_void, c_char, c_int, c_uint, c_ulong};

// consts
pub const RTAUDIO_FORMAT_SINT8 : c_ulong = 0x01;
pub const RTAUDIO_FORMAT_SINT16: c_ulong = 0x02;
pub const RTAUDIO_FORMAT_SINT24 : c_ulong = 0x04;
pub const RTAUDIO_FORMAT_SINT32 : c_ulong = 0x08;
pub const RTAUDIO_FORMAT_FLOAT32 : c_ulong = 0x10;
pub const RTAUDIO_FORMAT_FLOAT64 : c_ulong = 0x20;
pub const RTAUDIO_FLAGS_NONINTERLEAVED : c_uint =  0x1;
pub const RTAUDIO_FLAGS_MINIMIZE_LATENCY : c_uint = 0x2;
pub const RTAUDIO_FLAGS_HOG_DEVICE  : c_uint = 0x4;
pub const RTAUDIO_FLAGS_SCHEDULE_REALTIME : c_uint =  0x8;
pub const RTAUDIO_FLAGS_ALSA_USE_DEFAULT  : c_uint = 0x10;
pub const RTAUDIO_STATUS_INPUT_OVERFLOW  : c_uint = 0x1;
pub const RTAUDIO_STATUS_OUTPUT_UNDERFLOW : c_uint =  0x2;
pub const NUM_SAMPLE_RATES : c_int = 16;
pub const MAX_NAME_LENGTH : c_int = 512;

// enums
#[repr(C)]
#[derive(Copy, Clone)]
pub enum rtaudio_error {
    RTAUDIO_ERROR_WARNING,
    RTAUDIO_ERROR_DEBUG_WARNING,
    RTAUDIO_ERROR_UNSPECIFIED,
    RTAUDIO_ERROR_NO_DEVICES_FOUND,
    RTAUDIO_ERROR_INVALID_DEVICE,
    RTAUDIO_ERROR_MEMORY_ERROR,
    RTAUDIO_ERROR_INVALID_PARAMETER,
    RTAUDIO_ERROR_INVALID_USE,
    RTAUDIO_ERROR_DRIVER_ERROR,
    RTAUDIO_ERROR_SYSTEM_ERROR,
    RTAUDIO_ERROR_THREAD_ERROR
}

#[repr(C)]
#[derive(Copy, Clone)]
pub enum rtaudio_api {
    RTAUDIO_API_UNSPECIFIED,
    RTAUDIO_API_LINUX_ALSA,
    RTAUDIO_API_LINUX_PULSE,
    RTAUDIO_API_LINUX_OSS,
    RTAUDIO_API_UNIX_JACK,
    RTAUDIO_API_MACOSX_CORE,
    RTAUDIO_API_WINDOWS_WASAPI,
    RTAUDIO_API_WINDOWS_ASIO,
    RTAUDIO_API_WINDOWS_DS,
    RTAUDIO_API_DUMMY,
}

// structs
#[repr(C)]
#[derive(Copy,Clone)]
pub struct rtaudio_device_info {
    pub probed : c_int,
    pub output_channels : c_uint,
    pub input_channels : c_uint,
    pub duplex_channels : c_uint,
    pub is_default_output : c_int,
    pub is_default_input : c_int,
    pub native_formats : rtaudio_format_t,
    pub preferred_sample_rate : c_uint,
    pub sample_rates : [c_int; 16],
    pub name : [c_char; 512],
}

#[repr(C)]
pub struct rtaudio_stream_parameters {
    pub device_id : c_uint,
    pub num_channels : c_uint,
    pub first_channel : c_uint
}

#[repr(C)]
pub struct rtaudio_stream_options {
    pub flags : rtaudio_stream_flags_t,
    pub num_buffers : c_uint,
    pub priority : c_int,
    pub name : [c_char; 512]
}

// types
pub type rtaudio_device_info_t = rtaudio_device_info;
pub type rtaudio_stream_parameters_t = rtaudio_stream_parameters;
pub type rtaudio_stream_options_t = rtaudio_stream_options;
pub type rtaudio_t = *mut c_void;
pub type rtaudio_api_t = rtaudio_api;
pub type rtaudio_error_t = rtaudio_error;
pub type rtaudio_format_t = c_ulong;
pub type rtaudio_stream_flags_t = c_uint;
pub type rtaudio_stream_status_t = c_uint;

pub type rtaudio_cb_t = extern fn (out : *mut c_void, input : *mut c_void,
                                   nFrames : c_uint, stream_time : f64,
                                   status : rtaudio_stream_status_t,
                                   userdata : *mut c_void) -> c_int;

pub type rtaudio_error_cbk_t = extern fn (err : rtaudio_error_t, msg : *const c_char);

// functions
extern {
    pub fn rtaudio_version() -> *const c_char;
    pub fn rtaudio_compiled_api() -> *mut rtaudio_api_t;
    pub fn rtaudio_error(audio: rtaudio_t) -> *const c_char;
    pub fn rtaudio_create(api: rtaudio_api_t) -> rtaudio_t;
    pub fn rtaudio_destroy(audio : rtaudio_t);
    pub fn rtaudio_current_api (audio : rtaudio_t) -> rtaudio_api_t;
    pub fn rtaudio_device_count (audio :rtaudio_t) -> c_int;

    pub fn rtaudio_get_device_info (audio : rtaudio_t, i : c_int) -> rtaudio_device_info_t;
    pub fn rtaudio_get_default_output_device (audio : rtaudio_t) -> c_uint;
    pub fn rtaudio_get_default_input_device (audio : rtaudio_t) -> c_uint;

    pub fn rtaudio_open_stream (
        audio : rtaudio_t,
        output_params : *mut rtaudio_stream_parameters_t,
        input_params : *mut rtaudio_stream_parameters_t,
        format : rtaudio_format_t,
        sample_rate : c_uint,
        buffer_frames : *mut c_uint,
        cb : rtaudio_cb_t,
        userdata : *mut c_void,
        options : *mut rtaudio_stream_options_t,
        errcb : rtaudio_error_cbk_t
    ) -> c_int;

    pub fn rtaudio_close_stream(audio : rtaudio_t);
    pub fn rtaudio_start_stream(audio : rtaudio_t) -> c_int;
    pub fn rtaudio_stop_stream(audio : rtaudio_t) -> c_int;
    pub fn rtaudio_abort_stream(audio : rtaudio_t) -> c_int;

    pub fn rtaudio_is_stream_open(audio : rtaudio_t) -> c_int;
    pub fn rtaudio_is_stream_running (audio : rtaudio_t) -> c_int;

    pub fn rtaudio_get_stream_time (audio : rtaudio_t) -> f64;
    pub fn rtaudio_set_stream_time (audio : rtaudio_t, time : f64);
    pub fn rtaudio_get_stream_latency (audio : rtaudio_t) -> c_int;
    pub fn rtaudio_get_stream_sample_rate (audio : rtaudio_t) -> c_uint;
    pub fn rtaudio_show_warnings (audio : rtaudio_t, show : c_int);
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::ffi::CStr;
    use std::ptr::null_mut;

    fn info_test (api : rtaudio_api) {
        unsafe {
            let audio = rtaudio_create(api);
            for i in 0..rtaudio_device_count(audio) {
                let info = rtaudio_get_device_info(audio, i);
                println!("device #{}: \n {}", i, get_info(info));
            }
        }
    }

    fn get_info (info : rtaudio_device_info) -> String {
        let name = unsafe { CStr::from_ptr(info.name.as_ptr()) };
        let s = format!("name: {:?} \n ins: {:?} \n outs: {:?}",
                        name, info.input_channels, info.output_channels);
        s
    }

    #[cfg(all(feature = "core-audio", target_os = "macos"))]
    #[test]
    fn core_audio_info () {
        println!("CoreAudio devices ... ");
        info_test(rtaudio_api::RTAUDIO_API_MACOSX_CORE);
    }

    #[cfg(all(feature = "direct-sound", target_os = "windows"))]
    #[test]
    fn direct_sound_info () {
        println!("DirectSound devices ... ");
        info_test(rtaudio_api::RTAUDIO_API_WINDOWS_DS);
    }

    #[cfg(all(feature = "wasapi", target_os = "windows"))]
    #[test]
    fn wasapi_info () {
        println!("WASAPI devices ... ");
        info_test(rtaudio_api::RTAUDIO_API_WINDOWS_WASAPI);
    }

    struct Internal {
        counter : f32,
        was_called : bool
    }

    extern fn dummy_cbk (output : *mut c_void,
                         _input : *mut c_void,
                         num_frames : c_uint,
                         _stream_time : f64,
                         _status : rtaudio_stream_status_t,
                         user_data : *mut c_void) -> c_int {
        let output = unsafe {
            std::slice::from_raw_parts_mut (output as *mut f32, num_frames as usize)
        };

        let mut user_data = unsafe {
            (user_data as *mut Internal)
                .as_mut()
                .expect ("callback state invalid")
        };

        user_data.was_called = true;

        for n in 0..(num_frames as usize){
            use std::f32;

            user_data.counter += if user_data.counter > 1.0 {
                -1.0
            } else {
                440.0 / 48000.0
            };

            let x = user_data.counter;
            output[n] = 0.5 * (x * f32::consts::PI * 2.0).cos();
        }
        0
    }

    #[allow(unreachable_code)]
    fn test_api() -> rtaudio_t {
        unsafe {
            #[cfg(target_os = "macos")]
                return rtaudio_create(rtaudio_api::RTAUDIO_API_MACOSX_CORE);

            #[cfg(all(target_os = "windows", feature = "wasapi"))]
                return rtaudio_create(rtaudio_api::RTAUDIO_API_WINDOWS_WASAPI);

            #[cfg(all(target_os = "windows", not(feature = "wasapi")))]
                return rtaudio_create(rtaudio_api::RTAUDIO_API_WINDOWS_DS);

            rtaudio_create(rtaudio_api::RTAUDIO_API_DUMMY)
        }
    }

    #[test]
    fn sin_test() {
        let mut audio = test_api();

        println!("playing back through {}",
                 get_info(unsafe {
                     rtaudio_get_device_info (audio, rtaudio_get_default_output_device(audio) as i32)
                 }));

        let mut output_params = rtaudio_stream_parameters_t {
            device_id: unsafe { rtaudio_get_default_output_device(audio) },
            num_channels: 1,
            first_channel: 0
        };

        let mut stream_options = rtaudio_stream_options {
            flags : RTAUDIO_FLAGS_NONINTERLEAVED,
            num_buffers : 3,
            priority : RTAUDIO_FLAGS_SCHEDULE_REALTIME as i32,
            name : [0; 512]
        };

        let mut buffer_size: c_uint = 512;

        // This only works because the function initializing the callback exits after the last
        // callback, this is _very_ unsafe
        let mut user_data = Internal { counter : 0.0, was_called : false};

        unsafe {
            let ec = rtaudio_open_stream(audio,
                                         &mut output_params as *mut rtaudio_stream_parameters_t,
                                         null_mut(),
                                         RTAUDIO_FORMAT_FLOAT32,
                                         48000,
                                         (&mut buffer_size) as *mut c_uint,
                                         dummy_cbk,
                                         &mut user_data as *mut _ as *mut c_void,
                                         &mut stream_options as *mut rtaudio_stream_options_t,
                                         dummy_err);

            println!("{}", stream_options.flags);

            // the C API only returns -1 if the callback fails, so to debug you'll need to work
            // backwards when this fails.
            assert_eq!(ec, 0);

            assert_eq!(rtaudio_start_stream(audio), 0);
            std::thread::sleep(std::time::Duration::from_millis(5000));

            assert_eq!(rtaudio_stop_stream(audio), 0);
            std::thread::sleep(std::time::Duration::from_millis(100));

            assert_eq!(rtaudio_start_stream(audio), 0);
            std::thread::sleep(std::time::Duration::from_millis(5000));

            rtaudio_close_stream(audio);
            rtaudio_destroy(audio);
        }

        // if this fails, there was an internal error not caught by the stream functions
        // again, this only works because the stream will have be closed before this
        // code is reached.
        assert!(user_data.was_called);
    }

    extern fn dummy_err (_err : rtaudio_error_t, _msg : *const c_char) {}
}
