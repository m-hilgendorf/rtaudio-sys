#![allow(dead_code)]
extern crate cc;
use cc::Build;

#[cfg(target_os = "windows")]
fn windows (build : &mut Build) {
    println!("cargo:rustc-link-lib=winmm");
    println!("cargo:rustc-link-lib=ole32");

    #[cfg(feature="direct-sound")]
        {
            build.define("__WINDOWS_DS__","");
            println!("cargo:rustc-link-lib=dsound");
            println!("cargo:rustc-link-lib=user32");
        }

    #[cfg(feature="asio")]
        {
            build.define("__WINDOWS_ASIO__","");

            build.files ([
                "rtaudio/include/asio.cpp",
                "rtaudio/include/asiodrivers.cpp",
                "rtaudio/include/asiolist.cpp",
                "rtaudio/include/iasiothiscallresolver.cpp",
            ].iter());
        }

    #[cfg(feature="wasapi")]
        {
            build.define("__WINDOWS_WASAPI__","");

            println!("cargo:rustc-link-lib=ksuser");
            println!("cargo:rustc-link-lib=mfplat");
            println!("cargo:rustc-link-lib=mfuuid");
            println!("cargo:rustc-link-lib=wmcodecdspuuid");
        }
}

#[cfg(target_os = "macos")]
fn macos(build : &mut Build) {
    println!("cargo:rustc-link-lib=framework=CoreFoundation");
    println!("cargo:rustc-link-lib=framework=CoreAudio");
    println!("cargo:rustc-link-lib=c++");
    build.define("__MACOSX_CORE__","");
}
fn main() {
    let mut build = Build::new();

    build.files (["rtaudio/RtAudio.cpp", "rtaudio/rtaudio_c.cpp"].iter())
        .include("rtaudio/")
        .include("rtaudio/include");

    #[cfg(debug_assertions)]
        build.define("__RTAUDIO_DEBUG__", "");

    #[cfg(target_os = "windows")]
        windows (&mut build);

    #[cfg(target_os = "macos")]
        macos(&mut build);

    build.compile("rtaudio");
}